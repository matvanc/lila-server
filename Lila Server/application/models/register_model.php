<?php
//make it with one naming convention without _
class Register_model extends CI_Model {

	//good, use of String.format() function
	public function sign_up($nickname, $password, $first_name, $family_name, $date_of_birth, $country) {
		
		$user = array(
			'Nickname' => $nickname,
			'Password' => $password,
			'First_name' => $first_name,
			'Family_name' => $family_name,
			'Date_of_birth' => $date_of_birth,
			'Country' => $country
		);
		
		return $this->db->insert('User', $user);
	}
	
	public function is_registered($nickname, $password) {
		
		$query = $this->db
		->where('Nickname', $nickname)
		->where('Password', $password)
		->get('User');
		
		if ($query->num_rows() == 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public function login($nickname, $password) {
		if (!$this->is_registered($nickname, $password))
			return NULL;
			
		$query = $this->db
		->where('Nickname', $nickname)
		->get('Session');
		
		if ($query->num_rows() > 0) {
			return "LOGGED"; 
		}
		
		$sessionID = md5( $nickname.$password.date('m/d/Y h:i:s a', time()) );
		
		$user = array(
			'SessionID' => $sessionID,
			'Nickname' => $nickname
		);
		
		$this->db->insert('Session', $user);
		
		return ( ($this->db->affected_rows() == 0) ? NULL : $sessionID );
	}
	
	public function getUser($sessionID) {
		$query = $this->db
		->where('SessionID', $sessionID)
		->get('Session');
		
		if ($query->num_rows() == 0)
			return NULL;
		else
			return $query->row()->Nickname;
	}
	
	public function getSession($nickname) {
		$query = $this->db
		->where('Nickname', $nickname)
		->get('Session');
		
		if ($query->num_rows() == 0)
			return NULL;
		else
			return $query->row()->SessionID;
	}
	
	public function logout($sessionID) {
		$this->db->delete('Session', array('SessionID' => $sessionID));
		
		return ( ($this->db->affected_rows() == 0) ? false : true );
	}
	
	
	
	
	
	
	
	
	
	
	
	public function add_friend($sessionID, $fellow) {
		
		$follower = $this->getUser($sessionID);
		
		$fellowship = array(
			'Follower' => $follower,
			'Fellow' => $fellow
		);
		
		$this->db->insert('Fellowship', $fellowship);
		
		return ( ($this->db->affected_rows() == 1) ? true : false );
	}
	
	public function get_friends($sessionID) {
		
		$follower = $this->getUser($sessionID);
		
		if ($follower == NULL)
			return NULL;
		
		$query = $this->db
		->select('Fellow')
		->where('Follower', $follower)
		->get('Fellowship');
		
		
		$resultArray = array($follower);
		foreach ($query->result() as $row) {
			array_push($resultArray, $row->Fellow);
		}
		
		
		return $resultArray;
	}
	
	public function getUserInfo($sessionID, $field) {
		
		$nickname = $this->getUser($sessionID);
		
		$result = $this->db->where('Nickname', $nickname)->get('User')->row_array();
		
		if (array_key_exists($field, $result)) {
			return $result[$field];
		}
		else
			return NULL;
	}
	
	
	public function get_user_info($nickname, $field) {
		
		$result = $this->db->where('Nickname', $nickname)->get('User')->row_array();
		
		if (array_key_exists($field, $result)) {
			return $result[$field];
		}
		else
			return NULL;
	}	



	public function uploadDailyChallenge() {
		$author = $this->getUser($_POST['Session_ident']);
		
		$dailyChallenge = array(
			'Picture' => $_POST['Picture'],
			'Description' => $_POST['Description'],
			'Author' => $author,
			'Latitude' => $_POST['Latitude'],
			'Longitude' => $_POST['Longitude'],
			'Date_time' => $_POST['Date_time']
		);
		
		$this->db->insert('Daily_challenge', $dailyChallenge);
		
		return ( ($this->db->affected_rows() == 1) ? true : false );
	}
	
	public function getDailyChallenge($dailyChallenge) {
		
		$query = $this->db
		->where('ID', $dailyChallenge)
		->get('Daily_challenge');
		
		if ($query->num_rows() == 0)
			return NULL;
		else
			return $query->row();
	}
	
	
	
	
	
	
	
	
	
	//OK
	public function add_comment($sessionID, $dailyChallenge, $text) {
		
		$author = $this->getUser($sessionID);
		
		$user = array(
			'Daily_challenge' => $dailyChallenge,
			'Text' => $text,
			'Author' => $author
		);
		
		$this->db->insert('Comment', $user);
		
		return ( ($this->db->affected_rows() == 1) ? true : false );
	}
	
	
	public function get_comments($dailyChallenge) {
		$query = $this->db
		->from('Comment')
		->where('Daily_challenge', $dailyChallenge)
		->order_by('Date_time', 'asc')
		->get();
		
		if ($query->num_rows() == 0)
			return NULL;
		else
			return $query->result();		
	}
	
	
	public function get_user_daily_challenges($user) {
		$query = $this->db
		->select('*')
		->from('Daily_challenge')
		->join('User', 'User.Nickname = Daily_challenge.Author')
		->where('Author', $user)
		->order_by('Date_time', 'desc')
		->get(); 
		
		if ($query->num_rows() == 0)
			return NULL;
		else
			return $query->result();		
		
	}
	
	public function get_wall_daily_challenges($sessionID) {
		
		$friends = $this->get_friends($sessionID);
		
		$resultArray = array();
		foreach ($friends as $friend) {
			$daily_challenges = $this->get_user_daily_challenges($friend);
			if ($daily_challenges != null) {
				$daily_challenge = $daily_challenges[0];
				array_push($resultArray, $daily_challenge);
			}
		}
		
		if (count($resultArray) == 0)
			return NULL;
		else
			return $resultArray;				
	}
	
	

}

?>