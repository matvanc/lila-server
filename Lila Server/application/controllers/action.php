<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Action extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		header('Content-type: text/html; charset=UTF-8');
		$this->load->model('register_model');
	}
	
	public function test() {
		echo '<a href="http://mvanco.eu/subdom/lila/index.php/action/show_wall/6fc493e2bd41193515962d40237b93a2">show_wall</a><br>';
	}
		

	public function sign_up(
		$nickname, $password, $first_name, $family_name, $date_of_birth, $country = 'NONE'
	) {
		if ($this->register_model->sign_up(
			urldecode($nickname),
			urldecode($password),
			urldecode($first_name),
			urldecode($family_name),
			urldecode($date_of_birth),
			urldecode($country)
		)) {
			http_response_code(201); //success
		}
		else {
			http_response_code(404); //http success but for client information about nothing happened
		}

	}
	
	public function validate($nickname, $password) {
		
		if ( $this->register_model->validate($nickname, $password) )
			http_response_code(201);
		else
			http_response_code(404);
	}
	
	public function login($nickname, $password){
		$sessionID = $this->register_model->login($nickname, $password);
		if ( $sessionID ) {
			if ($sessionID == "LOGGED") { 
				http_response_code(200); 
				echo $this->register_model->getSession($nickname);
				
			}
			else {
				http_response_code(201);
				echo $sessionID;
			}
		}
		else
			http_response_code(404);
	}
	
	public function getUser($sessionID) {
		if ( $user = $this->register_model->getUser($sessionID) ) {
			http_response_code(201);
			echo $user;	
		}
		else
			http_response_code(404);
	}
	
	public function logout($sessionID) {
		$result = $this->register_model->logout($sessionID);
		
		if ( $result ) {
			
			
			http_response_code(201);

			
		}
		else
			http_response_code(404);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function add_friend($sessionID, $fellow) {
		if ($this->register_model->add_friend($sessionID, $fellow)) {
			http_response_code(201); //success
		}
		else {
			http_response_code(404); //http success but for client information about nothing happened
		}
		
	}
	
	public function get_friends($sessionID) {
		if ($friends = $this->register_model->get_friends($sessionID)) {
			http_response_code(201); //success
			echo var_export($friends, true);
		}
		else {
			http_response_code(404); //http success but for client information about nothing happened
		}
	}
	

	
	public function getUserInfo($sessionID, $field) {
		if ($info = $this->register_model->getUserInfo($sessionID, $field)) {
			http_response_code(201); //success
			
			echo $info;
		}
		else {
			http_response_code(404); //http success but for client information about nothing happened
		}
	}
	
	
	
	
	
	
	public function uploadDailyChallenge() {
		if ($friends = $this->register_model->uploadDailyChallenge()) {
			http_response_code(201); //success
		}
		else {
			http_response_code(404); //http success but for client information about nothing happened
		}
	}
	
	
	
	

	
	public function add_comment($sessionID, $dailyChallenge, $text) {
		if ($this->register_model->add_comment($sessionID, $dailyChallenge, $text)) {
			http_response_code(201); //success
		}
		else {
			http_response_code(404); //http success but for client information about nothing happened
		}
	}
	
	public function get_comments($dailyChallenge) {
		if ($result = $this->register_model->get_comments($dailyChallenge)) {
			http_response_code(201); //success
			foreach ($result as $row) {
				echo var_export($row, true);
			}
		}
		else {
			http_response_code(404); //http success but for client information about nothing happened
		}
	}
	
	public function get_daily_challenges($author) {
		if ($result = $this->register_model->get_daily_challenges($author)) {
			http_response_code(201); //success
			foreach ($result as $row) {
				echo var_export($row, true);
			}			
		}
		else {
			http_response_code(404); //http success but for client information about nothing happened
		}
	}	
	
	
	
	
	
	
	
	
	
	
	public function show_wall($sessionID) {
		if ($this->register_model->getUser($sessionID) == NULL) {
			http_response_code(404);
			return;
		}
		$data['sessionID'] = $sessionID;
		$result = $this->register_model->get_wall_daily_challenges($sessionID);
		
		if ($result != null) {
			$data['daily_challenges'] = $result;
			
			$this->load->view('templates/header', $data);
			$this->load->view('wall', $data);
			$this->load->view('templates/footer', $data);
		}
		else {
			$this->load->view('templates/header', $data);
			$this->load->view('blank_wall', $data);
			$this->load->view('templates/footer', $data);
		}
	}
	
	public function show_user($sessionID, $user) {
		
		if ($this->register_model->getUser($sessionID) == NULL) {
			http_response_code(404);
			return;
		}		
		
		$data['sessionID'] = $sessionID;
		$data['daily_challenges'] = $this->register_model->get_user_daily_challenges($user);
		
		$firstName = $this->register_model->get_user_info($user, 'First_name');
		$familyName = $this->register_model->get_user_info($user, 'Family_name');
		
		$data['title'] = $firstName.' '.$familyName.' ('.$user.')';

		$this->load->view('templates/header', $data);
		$this->load->view('daily_challenge_history', $data);
		$this->load->view('templates/footer', $data);
	}
	
	
 	public function show_picture($sessionID, $dailyChallenge) {
		if ($this->register_model->getUser($sessionID) == NULL) {
			http_response_code(404);
			return;
		}			
		
		$data['picture'] = $this->register_model->getDailyChallenge($dailyChallenge)->Picture;
		
		$this->load->view('show_picture', $data);	
	}

	
	public function show_daily_challenge($sessionID, $dailyChallenge) {
		
		if ($this->register_model->getUser($sessionID) == NULL) {
			http_response_code(404);
			return;
		}		
		
		$data['sessionID'] = $sessionID;
		$result = $this->register_model->getDailyChallenge($dailyChallenge);
		$data['daily_challenge'] = $result;
		
		$sessionID = $this->register_model->getSession($result->Author);
		
		$data['First_name'] = $this->register_model->getUserInfo($sessionID, 'First_name');		
		
		$data['Family_name'] = $this->register_model->getUserInfo($sessionID, 'Family_name');

		$result = $this->register_model->get_comments($dailyChallenge);
		$data['comments'] = $result;
		
		$this->load->view('templates/header', $data);
		$this->load->view('daily_challenge', $data);	
		$this->load->view('templates/footer', $data);	
	}
	
	public function show_comment_add_form($sessionID, $dailyChallenge) {
		$data['sessionID'] = $sessionID;
		$data['daily_challenge'] =$dailyChallenge;
		
		
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('comment', 'Comment', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('comment_form', $data);
			$this->load->view('templates/footer', $data);	
		}
		else
		{
			$this->register_model->add_comment($sessionID, $dailyChallenge,
				$this->input->post('comment'));
			
			$this->show_daily_challenge($sessionID, $dailyChallenge);
		}		
	}
	
	
}