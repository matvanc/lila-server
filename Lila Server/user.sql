DROP TABLE IF EXISTS Session;
DROP TABLE IF EXISTS Comment;
DROP TABLE IF EXISTS Daily_challenge;
DROP TABLE IF EXISTS Fellowship;
DROP TABLE IF EXISTS User;

CREATE TABLE User
(
Nickname VARCHAR(12) NOT NULL,
Password VARCHAR(12) NOT NULL,
First_name VARCHAR(20) NOT NULL,
Family_name VARCHAR(20) NOT NULL,
Date_of_birth DATE NOT NULL,
Country VARCHAR(50),
CONSTRAINT Pk_User PRIMARY KEY(Nickname)
);

CREATE TABLE Fellowship
(
ID INTEGER NOT NULL AUTO_INCREMENT,
Follower VARCHAR(12) NOT NULL,
Fellow VARCHAR(12) NOT NULL,
CONSTRAINT Pk_Fellowship PRIMARY KEY(ID),
CONSTRAINT Fk_Fellowship_Follower FOREIGN KEY(Follower) REFERENCES User(Nickname),
CONSTRAINT Fk_Fellowship_Fellow FOREIGN KEY(Fellow) REFERENCES User(Nickname)
);

CREATE TABLE Daily_challenge
(
ID INTEGER NOT NULL AUTO_INCREMENT,
Picture LONGBLOB NOT NULL,
Description VARCHAR(50) NOT NULL,
Author VARCHAR(12) NOT NULL,
Latitude VARCHAR(10),
Longitude VARCHAR (10),
Date_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT Pk_DChallenge PRIMARY KEY(ID),
CONSTRAINT Fk_DChallenge_Author FOREIGN KEY(Author) REFERENCES User(Nickname)
);

CREATE TABLE Comment
(
ID INTEGER NOT NULL AUTO_INCREMENT,
Daily_challenge INTEGER NOT NULL,
Text VARCHAR(100) NOT NULL,
Author VARCHAR(12) NOT NULL,
Date_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT Pk_Comment PRIMARY KEY(ID),
CONSTRAINT Fk_Comment_DChallenge FOREIGN KEY(Daily_challenge) REFERENCES Daily_challenge(ID),
CONSTRAINT Fk_Comment_Author FOREIGN KEY(Author) REFERENCES User(Nickname)
);

CREATE TABLE Session
(
SessionID VARCHAR(32) NOT NULL,
Nickname VARCHAR(12) NOT NULL,
Start_date_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT Pk_Online PRIMARY KEY(SessionID),
CONSTRAINT Fk_Online_Nickname FOREIGN KEY(Nickname) REFERENCES User(Nickname)
);

